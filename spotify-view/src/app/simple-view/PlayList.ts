import {PlaylistTracksRef} from './PlaylistTracksRef';
import {ExternalUrl} from './ExternalUrl';

export class Item {
    collaborative: boolean;
    description: string;
    externalUrls: ExternalUrl;
    href: string;
    id: number;
    name: string;
    owner: string;
    public: boolean;
    tracks: PlaylistTracksRef;
    type: string;
    uri: string;
}
