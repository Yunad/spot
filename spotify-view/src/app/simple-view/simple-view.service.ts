import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from "rxjs";
import {UserBrowser} from "./UserBrowser";
import {User} from "./User";

const PLAYLIST_URL = environment.playlistUrl;
const LOGIN_URL = environment.loginUrl;
const USER_DETAILS = environment.userDetails;
const headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
});

@Injectable({
    providedIn: 'root'
})
export class SimpleViewService {

    constructor(private httpClient: HttpClient) {
    }

    click(userBrowser: UserBrowser): Observable<any> {
        console.log(userBrowser)
        return this.httpClient.post('http://localhost:8080/api/v1/login', userBrowser);
    }

    getUserInfo(userBrowser: UserBrowser):Observable<User> {
        return this.httpClient.post<User>(USER_DETAILS, userBrowser)
    }
}