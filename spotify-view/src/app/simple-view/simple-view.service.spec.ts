import { TestBed } from '@angular/core/testing';

import { SimpleViewService } from './simple-view.service';

describe('SimpleViewService', () => {
  let service: SimpleViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SimpleViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
