import {Component, OnInit} from '@angular/core';
import {SimpleViewService} from './simple-view.service';
import FingerprintJS from "@fingerprintjs/fingerprintjs";
import {UserBrowser} from "./UserBrowser";
import {User} from "./User";

let result;
(async () => {
    // We recommend to call `load` at application startup.
    const fp = await FingerprintJS.load();

    // The FingerprintJS agent is ready.
    // Get a visitor identifier when you'd like to.
    result = await fp.get();

    // This is the visitor identifier:
    const visitorId = result.visitorId;
    console.log(result)
    console.log(visitorId);
})();

@Component({
    selector: 'app-simple-view',
    templateUrl: './simple-view.component.html',
    styleUrls: ['./simple-view.component.css']
})
export class SimpleViewComponent implements OnInit {
    private userBrowser: UserBrowser;
    private userUrl: any;
    user: User;

    constructor(private simpleViewService: SimpleViewService) {
    }

    ngOnInit(): void {
    }

    login() {
        this.userBrowser = new UserBrowser();
        this.userBrowser.userBrowserId = result.visitorId;
        let observable = this.simpleViewService.click(this.userBrowser);
        observable.subscribe(
            value => {
                console.log(value)
                window.location.replace(value.uri)
            }, error => console.log(error)
        )
    }

    getUserInfo() {
        this.userBrowser = new UserBrowser();
        this.userBrowser.userBrowserId = result.visitorId;
        this.simpleViewService.getUserInfo(this.userBrowser)
            .subscribe(
                value => {
                    console.log(value);
                    this.user = value;
                },
                error => console.log(error),
            )
    }
}