// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const API_URL = 'http://localhost:8080/api/v1';

export const environment = {
    production: false,
    playlistUrl: API_URL.concat('/user_playlists'),
    loginUrl: API_URL.concat('/login'),
    userDetails: API_URL.concat('/user-details')
};
