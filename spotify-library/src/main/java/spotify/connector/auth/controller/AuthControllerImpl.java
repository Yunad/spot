package spotify.connector.auth.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import spotify.connector.LoginRedirect;
import spotify.connector.SpotifyClient;
import spotify.connector.UserBrowserDTO;

@RestController
@Log4j2
public class AuthControllerImpl implements AuthController {
    private final SpotifyClient spotifyClient;

    public AuthControllerImpl(SpotifyClient spotifyClient) {
        this.spotifyClient = spotifyClient;
    }

    @Override
    public LoginRedirect spotifyLogin(@RequestBody UserBrowserDTO userBrowserDTO) {
        log.debug("AuthController spotify Login: user Browser: {}", userBrowserDTO);
        return new LoginRedirect(spotifyClient.getAuthorizationUri(userBrowserDTO.getUserBrowserId()));
    }

    @Override
    public void getCallback(@RequestParam("code") String code) {
        log.debug("AuthController getCallback");
        log.debug("Handling authorization callback with code: {}", code);
        spotifyClient.authorizationCallback(code);
    }
}