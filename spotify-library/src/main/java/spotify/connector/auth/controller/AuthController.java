package spotify.connector.auth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import spotify.connector.AbstractApiController;
import spotify.connector.LoginRedirect;
import spotify.connector.UserBrowserDTO;

import static org.springframework.http.HttpStatus.OK;

public interface AuthController extends AbstractApiController {

    @PostMapping(value = "login", consumes = "application/json")
    LoginRedirect spotifyLogin(@RequestBody UserBrowserDTO userBrowserDTO);

    @GetMapping("callback")
    @ResponseStatus(OK)
    void getCallback(@RequestParam("code") String code);
}