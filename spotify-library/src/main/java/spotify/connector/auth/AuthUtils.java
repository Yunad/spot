package spotify.connector.auth;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import spotify.connector.dto.AboutMeDTO;
import spotify.connector.dto.UserAuthorizationResponseDTO;

import java.util.Base64;


@Component
@Log4j2
public class AuthUtils {
    private static final String SPOTIFY_URI_TOKEN = "https://accounts.spotify.com/api/token";
    private static final String SPOTIFY_URI_ABOUT_ME = "https://api.spotify.com/v1/me";
    @Value("${spotify.client.id}")
    public String clientId;
    @Value("${spotify.client.secret.id}")
    public String clientSecretId;
    @Value("${spotify.uri.authorization.callback}")
    private String uriAuthorizationCallback;

    public Flux<UserAuthorizationResponseDTO> makeAPostCall(String code) {
        return WebClient.create()
                .post()
                .uri(SPOTIFY_URI_TOKEN)
                .body(BodyInserters
                        .fromFormData("grant_type", "authorization_code")
                        .with("code", code)
                        .with("redirect_uri", uriAuthorizationCallback))
                .headers(httpHeaders -> httpHeaders.add("Authorization", "Basic " + getEncodedSpotifyAccess()))
                .retrieve()
                .bodyToFlux(UserAuthorizationResponseDTO.class);
    }

    private String getEncodedSpotifyAccess() {
        String s = this.clientId + ":" + this.clientSecretId;
        return Base64.getEncoder().encodeToString(s.getBytes());
    }

    public Flux<AboutMeDTO>  makeGetUserInfoCall(String token) {
        return WebClient.create()
                .get()
                .uri(SPOTIFY_URI_ABOUT_ME)
                .header("Authorization", "Bearer " + token)
                .retrieve()
                .bodyToFlux(AboutMeDTO.class);
    }
}