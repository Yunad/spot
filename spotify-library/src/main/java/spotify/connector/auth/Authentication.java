package spotify.connector.auth;

import reactor.core.publisher.Flux;
import spotify.connector.dto.AboutMeDTO;

import java.net.URI;

public interface Authentication {
    URI getAuthorizationUri(String userBrowserId);

    void authorizationCallback(String code);

    Flux<AboutMeDTO> getUserInfo(String userBrowserId);
}