package spotify.connector.auth;

import spotify.connector.SpotifyScope;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class SpotifyAuthData {
    private final String protocol;
    private final String port;
    private final String url;
    private final String clientId;
    private final String clientSecret;
    private final String responseType;
    private final URI redirectUri;
    private final String accessToken;
    private final String refreshToken;
    private final String state;
    private final boolean showDialog;
    private final List<SpotifyScope> scopeList;

    private SpotifyAuthData(Builder builder) {
        this.protocol = builder.protocol;
        this.port = builder.port;
        this.url = builder.url;
        this.clientId = builder.clientId;
        this.clientSecret = builder.clientSecret;
        this.responseType = builder.responseType;
        this.redirectUri = builder.redirectUri;
        this.accessToken = builder.accessToken;
        this.refreshToken = builder.refreshToken;
        this.state = builder.state;
        this.scopeList = builder.scopeList;
        this.showDialog = builder.showDialog;
    }

    public static String authorizationUrl(SpotifyAuthData spotifyAuthData) {
        String authorizationUrl = spotifyAuthData.getUrl();
        authorizationUrl = authorizationUrl.concat("client_id=" + spotifyAuthData.getClientId());
        authorizationUrl = authorizationUrl.concat("&response_type=" + spotifyAuthData.getResponseType());
        for (SpotifyScope scope : spotifyAuthData.getScopeList()) {
            authorizationUrl = authorizationUrl.concat("&scope=" + SpotifyScope.translateEnumToSpotifyScope(scope));
        }
        authorizationUrl = authorizationUrl.concat("&redirect_uri=" + spotifyAuthData.getRedirectUri());
        authorizationUrl = authorizationUrl.concat("&state=" + spotifyAuthData.getState());
        authorizationUrl = authorizationUrl.concat("&show_dialog=" + spotifyAuthData.isShowDialog());
        return authorizationUrl;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getState() {
        return state;
    }

    public String getPort() {
        return port;
    }

    public List<SpotifyScope> getScopeList() {
        return scopeList;
    }

    public String getUrl() {
        return url;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getResponseType() {
        return responseType;
    }

    public URI getRedirectUri() {
        return redirectUri;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public static class Builder {
        private boolean showDialog;
        private List<SpotifyScope> scopeList;
        private String protocol;
        private String port;
        private String url;
        private String clientId;
        private String clientSecret;
        private String responseType;
        private URI redirectUri;
        private String accessToken;
        private String refreshToken;
        private String state;

        public Builder setProtocol(String protocol) {
            this.protocol = protocol;
            return this;
        }

        public Builder setScopeList(List<SpotifyScope> scopeList) {
            this.scopeList = scopeList;
            return this;
        }

        public Builder setPort(String port) {
            this.port = port;
            return this;
        }

        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        public Builder setClientId(String clientId) {
            this.clientId = clientId;
            return this;
        }

        public Builder setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
            return this;
        }

        public Builder setResponseType(String responseType) {
            this.responseType = responseType;
            return this;
        }

        public Builder setRedirectUri(URI redirectUri) {
            this.redirectUri = redirectUri;
            return this;
        }

        public Builder setState(String state) {
            this.state = state;
            return this;
        }

        public Builder setScope(List<SpotifyScope> scope) {
            scopeList = new ArrayList<>(scope);
            return this;
        }

        public Builder setAccessToken(String accessToken) {
            this.accessToken = accessToken;
            return this;
        }

        public Builder setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
            return this;
        }

        public Builder setShowDialog(boolean showDialog) {
            this.showDialog = showDialog;
            return this;
        }

        public SpotifyAuthData build() {
            return new SpotifyAuthData(this);
        }
    }
}