package spotify.connector.auth;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import spotify.connector.UserAuthRepository;
import spotify.connector.dto.AboutMeDTO;
import spotify.connector.dto.UserAuthorizationResponseDTO;

import java.io.Serializable;
import java.net.URI;
import java.util.Arrays;

import static spotify.connector.SpotifyScope.USER_READ_EMAIL;
import static spotify.connector.SpotifyScope.USER_READ_PRIVATE;

@Service
@Log4j2
public class AuthenticationService implements Authentication, Serializable {
    private static final long serialVersionUID = 112412512412L;
    private final AuthUtils authUtils;
    private final UserAuthRepository userAuthRepository;
    private UserAuthorizationResponseDTO userAuthorizationResponseDTO;
    private AboutMeDTO aboutMeDTO;
    @Value("${spotify.uri.authorization.callback}")
    private String uriAuthorizationCallback;
    private String userBrowserId;

    public AuthenticationService(AuthUtils authUtils, UserAuthRepository userAuthRepository) {
        this.authUtils = authUtils;
        this.userAuthRepository = userAuthRepository;
    }

    @Override
    public URI getAuthorizationUri(String userBrowserId) {
        String authorizationUrl = getCreatedUrl();
        this.userBrowserId = userBrowserId;
        return URI.create(authorizationUrl);
    }

    @Override
    public void authorizationCallback(String code) {
        Flux<UserAuthorizationResponseDTO> userAuthorizationResponseDTOFlux = authUtils.makeAPostCall(code);
        userAuthorizationResponseDTOFlux.subscribe(info -> {
            info.setUserBrowserId(userBrowserId);
            userAuthRepository.save(info);
        });
    }

    @Override
    public Flux<AboutMeDTO> getUserInfo(String userBrowserId) {
        log.debug("user browser id {}", userBrowserId);
        UserAuthorizationResponseDTO byBrowserId = userAuthRepository.findByBrowserId(userBrowserId);
        log.info(byBrowserId);
        return authUtils.makeGetUserInfoCall(byBrowserId.getToken());
    }

    private String getCreatedUrl() {
        SpotifyAuthData spotifyAuthData = getSpotifyAuthData();
        return SpotifyAuthData.authorizationUrl(spotifyAuthData);
    }

    private SpotifyAuthData getSpotifyAuthData() {
        return new SpotifyAuthData
                .Builder()
                .setClientId(authUtils.clientId)
                .setResponseType("code")
                .setShowDialog(true)
                .setUrl("https://accounts.spotify.com/authorize?")
                .setRedirectUri(URI.create(uriAuthorizationCallback))
                .setState(RandomStringUtils.randomAlphabetic(1, 16))
                .setScope(Arrays.asList(USER_READ_PRIVATE, USER_READ_EMAIL))
                .build();
    }
}