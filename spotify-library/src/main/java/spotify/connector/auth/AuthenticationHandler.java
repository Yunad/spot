package spotify.connector.auth;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import spotify.connector.dto.AboutMeDTO;

import java.io.Serializable;
import java.net.URI;

@Log4j2
@Service
public class AuthenticationHandler implements Authentication, Serializable {

    private final AuthenticationService authService;

    public AuthenticationHandler(AuthenticationService authService) {
        this.authService = authService;
    }

    public URI getAuthorizationUri(String userBrowserId) {
        return authService.getAuthorizationUri(userBrowserId);
    }

    @Override
    public void authorizationCallback(String code) {
        authService.authorizationCallback(code);
    }

    public Flux<AboutMeDTO> getUserInfo(String userBrowserId) {
        return authService.getUserInfo(userBrowserId);
    }
}