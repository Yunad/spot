package spotify.connector;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import spotify.connector.auth.AuthenticationHandler;
import spotify.connector.dto.AboutMeDTO;

import java.net.URI;

@Component
@Log4j2
public class SpotifyClient {
    private final AuthenticationHandler authHandler;

    public SpotifyClient(AuthenticationHandler authHandler) {
        this.authHandler = authHandler;
    }


    public Flux<AboutMeDTO> getUserInfo(String userBrowserId) {
        log.debug("spotifyClieny getuserInfo");
        return authHandler.getUserInfo(userBrowserId);
    }

    public URI getAuthorizationUri(String userBrowserId) {
        log.debug("spotifyClient getAuthorizationUri");
        return authHandler.getAuthorizationUri(userBrowserId);
    }

    public void authorizationCallback(String code) {
        log.debug("spotifyClient getAuthorizationcallback");
        authHandler.authorizationCallback(code);
    }
}