package spotify.connector;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URI;

@Data
@AllArgsConstructor
public class LoginRedirect {
    private URI uri;
}