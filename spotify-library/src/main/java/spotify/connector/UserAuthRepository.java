package spotify.connector;

import spotify.connector.dto.UserAuthorizationResponseDTO;

import java.util.Map;

public interface UserAuthRepository {
    void save(UserAuthorizationResponseDTO userAuthorizationResponseDTO);

    Map<String, UserAuthorizationResponseDTO> findAll();

    UserAuthorizationResponseDTO findById(String id);

    UserAuthorizationResponseDTO findByBrowserId(String browserId);

    void update(UserAuthorizationResponseDTO userAuthorizationResponseDTO);

    void delete(String id);
}