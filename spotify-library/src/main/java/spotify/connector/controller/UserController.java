package spotify.connector.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Flux;
import spotify.connector.AbstractApiController;
import spotify.connector.UserBrowserDTO;
import spotify.connector.dto.AboutMeDTO;

public interface UserController extends AbstractApiController {
    @PostMapping("user-details")
    Flux<AboutMeDTO> getUserDetails(@RequestBody UserBrowserDTO userBrowserDTO);
}