package spotify.connector.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import spotify.connector.UserBrowserDTO;
import spotify.connector.UserService;
import spotify.connector.dto.AboutMeDTO;

@Log4j2
@RestController
public class UserControllerImpl implements UserController {
    private final UserService userService;

    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Flux<AboutMeDTO> getUserDetails(@RequestBody UserBrowserDTO userBrowserDTO) {
        log.debug("User-details controller");
        Flux<AboutMeDTO> userInfo = userService.getUserInfo(userBrowserDTO.getUserBrowserId());
        log.info(userInfo);
        return userInfo;
    }
}