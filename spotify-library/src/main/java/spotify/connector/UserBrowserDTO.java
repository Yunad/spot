package spotify.connector;

import lombok.Data;

@Data
public class UserBrowserDTO {
    private String userBrowserId;
}