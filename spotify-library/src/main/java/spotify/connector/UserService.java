package spotify.connector;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import spotify.connector.dto.AboutMeDTO;

@Service
public class UserService {
    private final SpotifyClient spotifyClient;

    public UserService(SpotifyClient spotifyClient) {
        this.spotifyClient = spotifyClient;
    }

    public Flux<AboutMeDTO> getUserInfo(String userBrowserId) {
        return spotifyClient.getUserInfo(userBrowserId);
    }
}