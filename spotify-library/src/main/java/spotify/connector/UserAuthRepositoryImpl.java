package spotify.connector;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import spotify.connector.dto.UserAuthorizationResponseDTO;

import java.util.Map;

@Repository
public class UserAuthRepositoryImpl implements UserAuthRepository {
    private final HashOperations hashOperations;

    public UserAuthRepositoryImpl(RedisTemplate<String, UserAuthorizationResponseDTO> redisTemplate) {
        this.hashOperations = redisTemplate.opsForHash();
    }

    public void save(UserAuthorizationResponseDTO userAuthorizationResponseDTO) {
        this.hashOperations.put("User", userAuthorizationResponseDTO.getUserBrowserId(), userAuthorizationResponseDTO);
    }

    public Map<String, UserAuthorizationResponseDTO> findAll() {
        return this.hashOperations.entries("User");
    }

    public UserAuthorizationResponseDTO findByBrowserId(String browserId) {
        return (UserAuthorizationResponseDTO) hashOperations.get("User", browserId);
    }

    public UserAuthorizationResponseDTO findById(String id) {
        return (UserAuthorizationResponseDTO) hashOperations.get("User", id);
    }

    public void update(UserAuthorizationResponseDTO userAuthorizationResponseDTO) {
        this.save(userAuthorizationResponseDTO);
    }

    public void delete(String id) {
        this.hashOperations.delete("User", id);
    }
}