package spotify.connector;

import org.apache.commons.lang3.StringUtils;

public enum SpotifyScope {
    //    Images
    UGC_IMAGE_UPLOAD,
    //    Listening History
    USER_READ_RECENTLY_PLAYED, USER_TOP_READ, USER_READ_PLAYBACK_POSITION,
    //    Spotify Connect
    USER_READ_PLAYBACK_STATE, USER_MODIFY_PLAYBACK_STATE, USER_READ_CURRENTLY_PLAYING,
    //            Playback
    APP_REMOTE_CONTROL, STREAMING,
    //            Playlists
    PLAYLIST_MODIFY_PUBLIC, PLAYLIST_MODIFY_PRIVATE, PLAYLIST_READ_PRIVATE, PLAYLIST_READ_COLLABORATIVE,
    //            Follow
    USER_FOLLOW_MODIFY, USER_FOLLOW_READ,
    //            Library
    USER_LIBRARY_MODIFY, USER_LIBRARY_READ,
    //            Users
    USER_READ_EMAIL, USER_READ_PRIVATE;

    public static String translateEnumToSpotifyScope(SpotifyScope scopeToTranslate) {
        String scopeToProcess;
        scopeToProcess = StringUtils.replace(String.valueOf(scopeToTranslate), "_", "-");
        scopeToProcess = StringUtils.lowerCase(scopeToProcess);
        return scopeToProcess;
    }
}
